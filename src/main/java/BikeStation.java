public class BikeStation {

	private String id;
	private String district;
	private String lon;
	private String lat;
	private String bikes;
	private String slots;
	private String zip;
	private String Address;
	private String addressNumber;
	private String nearbyStations;
	private String status;
	private String name;
	private String stationType;
	
	
	
	public BikeStation(String id, String district, String lon, String lat, String bikes, String slots, String zip,
			String address, String addressNumber, String nearbyStations, String status, String name,
			String stationType) {
		super();
		this.id = id;
		this.district = district;
		this.lon = lon;
		this.lat = lat;
		this.bikes = bikes;
		this.slots = slots;
		this.zip = zip;
		this.Address = address;
		this.addressNumber = addressNumber;
		this.nearbyStations = nearbyStations;
		this.status = status;
		this.name = name;
		this.stationType = stationType;
	}
	
	
	// Empty constructor is useful than the full one
	public BikeStation() {
		super();
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getBikes() {
		return bikes;
	}
	public void setBikes(String bikes) {
		this.bikes = bikes;
	}
	public String getSlots() {
		return slots;
	}
	public void setSlots(String slots) {
		this.slots = slots;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		this.Address = address;
	}
	public String getAddressNumber() {
		return addressNumber;
	}
	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}
	public String getNearbyStations() {
		return nearbyStations;
	}
	public void setNearbyStations(String nearbyStations) {
		this.nearbyStations = nearbyStations;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStationType() {
		return stationType;
	}
	public void setStationType(String stationType) {
		this.stationType = stationType;
	}
}
