import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.thinkopen.utils.WebPage;


public class LeggiJSON {

	protected final static Logger LOGGER = Logger.getLogger(LeggiJSON.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BasicConfigurator.configure();
		LOGGER.info("Start main method");
		
		WebPage wp = new WebPage( );
		
		wp.DownloadWP(Constants.URL, Constants.fileName); //I have the JSON and stored in the folder project
		
		ObjectMapper mapper = new ObjectMapper();	
		String json_String = wp.leggi(Constants.pathWorkSpace, Constants.fileName);
		
		
				
		try {
			BikeStation[] stations = mapper.readValue(json_String, BikeStation[].class);
			
			for(BikeStation bs : stations) {
				
				LOGGER.debug("Address: "+ bs.getAddress());
				
			}
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
