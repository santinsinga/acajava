package it.thinkopen.utils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class WebPage {

	protected final static Logger LOGGER = Logger.getLogger(WebPage.class);
	
	public void DownloadWP(String url, String fileName) {
		LOGGER.info("start download");
		try {
			URL website = new URL(url);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String leggi(String path, String nome) {
		LOGGER.info("Create String");
		Scanner file;
		String ris = "";
		try {
			file = new Scanner(new File(path+nome));
			while(file.hasNext()) {
				ris = ris + file.nextLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ris;
	}
	
	
}
